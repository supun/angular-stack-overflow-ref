import { Component } from '@angular/core';
import { TrainService } from './service/train-service';
import { Train } from './model/train';
import { ResponseWrapper } from './model/response-wrapper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  trainList : Train[];

  constructor(private trainService: TrainService) {
    this.trainService.findTrainPage()
        .subscribe(data =>{
        const  resultsWrapper:ResponseWrapper<Train> = data;
        this.trainList = resultsWrapper.data;
        });
       //   error => this.errorMessage = error);
  }
}
