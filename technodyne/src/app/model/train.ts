import { TrainInterface } from './train-interface';

export class Train implements TrainInterface {
  trainGUID: string;
  trainSymbol: string;
  createTime: string; //Should be formatted as a date, SQL table uses smalldatetime
  departTime: string;
  // origStatus: OriginalStatuses;
  badGUIDFlag: boolean;
  trips: any[];

  // private _evalStatus: EvalStatuses;
  // get evalStatus(): EvalStatuses
  // {
  //   return this.trips.reduce((min, p) => p.evalStatus < min ? p.evalStatus : min, this.trips[0].evalStatus);
  // }
}
