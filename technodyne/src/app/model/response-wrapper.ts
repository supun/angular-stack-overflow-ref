export declare class ResponseWrapper<T> {
  static SUCCESS: number;
  data: Array<T>;
}
