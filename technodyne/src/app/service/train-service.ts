import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions } from '@angular/http';
import { ResponseWrapper } from '../model/response-wrapper';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TrainService {
  constructor(private http: Http) {
  }

  findTrainPage(filter = '', sortOrder = 'desc', pageNumber = 0, pageSize = 15, departStart = null, departEnd = null, origStatusWhitelist = null): Observable<any> {
    //retrieves a page of trains to display in the train list
    // let url = AppUtils.backendApiUrl() + '/trainList/getPage';
    const url = 'assets/mockdata.json';
    let requestBody = {
      'filter': filter,
      'sortOrder': sortOrder,
      'pageNum': pageNumber,
      'pageSize': pageSize,
      'departStart': departStart,
      'departEnd': departEnd,
      'origStatusWhitelist': origStatusWhitelist
    };
    let options = new RequestOptions();
    return this.http.get(url)
               .map(res => res.json())
               .catch(error => Observable.throw(error.json()));
  }


}
